MKITTOOL
========

Developer's toolkit for MKit.

This package contains several tools intended to help create, build and
manage MKit projects.

So far, only build helpers are included: one for .rpm's and one for
.deb's.  Also their interface and feature set is not consistent; at some
point there is intent to make them behave in a more similar manner.
