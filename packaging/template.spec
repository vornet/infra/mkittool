%global sfincb %{_datadir}/shellfu/include-bash
%global shellfu_req shellfu >= __VDEP_SHELLFU_GE__, shellfu < __VDEP_SHELLFU_LT__
%global saturnin_req shellfu-bash-saturnin >= __VDEP_SATURNIN_GE__, shellfu-bash-saturnin < __VDEP_SATURNIN_LT__

Name:           __MKIT_PROJ_PKGNAME__
Version:        __MKIT_PROJ_VERSION__
Release:        1%{?dist}
Summary:        __MKIT_PROJ_NAME__ - __MKIT_PROJ_TAGLINE__
URL:            __MKIT_PROJ_VCS_BROWSER__
License:        GPLv2
Source0:        %{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  make

Requires:   %saturnin_req
Requires:   %shellfu_req
Requires:   make
Requires:   rpmdevtools
Requires:   shellfu-bash-pretty
Requires:   shellfu-bash-mkittool == %{version}
Requires:   shellfu-sh-isa
%description
__MKITTOOL_MKITDESC__

__MKITTOOL_MAINDESC__

%package -n shellfu-bash-mkittool
Summary: Shellfu/Bash modules for mkittool
Requires: %shellfu_req
Requires: rpm
Requires: shellfu-bash
Requires: shellfu-bash-pretty
%description -n shellfu-bash-mkittool
__MKIT_MAINDESC__

__MKITTOOL_MAINDESC__

__MKITTOOL_MODDESC__

%prep
%setup -q


%build
make %{?_smp_mflags} PREFIX=/usr

%install
%make_install PREFIX=/usr
rm "%{buildroot}/%{_libexecdir}/%{name}/%{name}-debstuff"

%files
%dir %{_datadir}/%{name}
%dir %{_libexecdir}/%{name}
%doc %{_docdir}/%{name}/README.md
%{_bindir}/%{name}
%{_datadir}/%{name}/complete/rpmstuff.sh
%{_datadir}/bash-completion/completions/%{name}
%{_libexecdir}/%{name}/%{name}-pokecopr
%{_libexecdir}/%{name}/%{name}-rpmstuff

%files -n shellfu-bash-mkittool
%{sfincb}/mkittool.sh

%changelog

# specfile built with MKit __MKIT_MKIT_VERSION__
