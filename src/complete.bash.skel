#!/bin/bash

__SATURNIN_COMPLETE_CMDNAME=__MKIT_PROJ_PKGNAME__
__SATURNIN_COMPLETE_M_OPTS="-D -V -d -h -v --full-debug --help --verbose --debug --version --version-semver"

#shellcheck disable=SC2016
__SATURNIN_COMPLETE_CODE='#!/bin/bash

__'"$__SATURNIN_COMPLETE_CMDNAME"'() {
    local Meta=$1       # current metacommand
    local Partial=$2    # current unfinished word
    local Previous=$3   # word preceding the unfinished one
    local ScmdWords     # words from subcommand on, if already present
    local Scmds=()      # all subcommands
    local parser=__${Meta}__scparse    # parse util function
    local asker=__${Meta}__scask    # util function to ask Saturnin
    local comptext      # compgen result text
    mapfile -t Scmds <<<"$("$Meta" --saturnin-get-subcommands)"
    COMPREPLY=()
    if $parser HAS; then
        COMP_WORDBREAKS=" "
        mapfile -t ScmdWords <<<"$($parser WORDS)"
        comptext=$(compgen -W "$($asker)" -- "$Partial")
    else
        case $Partial in
            -*)
                comptext=$(
                    compgen -W "'"$__SATURNIN_COMPLETE_M_OPTS"'" -- "$Partial"
                )
                ;;
            *)
                comptext=$(compgen -W "${Scmds[*]}" "$Partial")
        esac
    fi
    case $comptext in
        "") COMPREPLY=() ;;
        *)  mapfile -t COMPREPLY <<<"$comptext" ;;
    esac
    {
        declare -p Meta
        declare -p Partial
        declare -p Previous
        declare -p ScmdWords
        declare -p comptext
        declare -p COMPREPLY
    } | "__${Meta}__debug"
}

__'"$__SATURNIN_COMPLETE_CMDNAME"'__debug() {
    #
    # Send stdin to debug file, or /dev/null if debug is off
    #
    local dfile=/dev/null
    if test "$__SATURNIN_COMPLETE_DEBUG" == true; then
        dfile=/tmp/__saturnin_complete-'"$__SATURNIN_COMPLETE_CMDNAME"'.debug
    fi
    cat >"$dfile"
}

__'"$__SATURNIN_COMPLETE_CMDNAME"'__scask() {
    #
    # Ask and pass args from function plus all the filtered words
    #
    "$Meta" --saturnin-sccompgen "$Meta" "$Partial" "$Previous" "${ScmdWords[@]}"
}

__'"$__SATURNIN_COMPLETE_CMDNAME"'__scparse() {
    local what=$1
    set -- "${COMP_WORDS[@]}"
    test "$1" == "$Meta" || echo "__${Meta}__scparse(): completion error" >&2
    shift
    local scswgaps=" ${Scmds[*]} "
    while true; do
        test -n "$1" || return 1
        test "${1#-}" == "$1" || { shift; continue; }
        test "${scswgaps/ $1 /}" == "$scswgaps" && { shift; continue; }
        break
    done
    case $what in
        HAS)    : ;;
        WORDS)  echo "$1"
                shift
                while test $# -gt 0; do
                    echo "$1"
                    shift
                done
                ;;
    esac
    return 0
}
'

eval "$__SATURNIN_COMPLETE_CODE"

complete -F "__$__SATURNIN_COMPLETE_CMDNAME" "$__SATURNIN_COMPLETE_CMDNAME"
