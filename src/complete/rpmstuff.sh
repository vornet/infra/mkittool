#!/bin/bash

_saturnin__sccompgen_handle() {
    case $_SATURNIN_SCCOMPGEN__POS:$_SATURNIN_SCCOMPGEN__PREVIOUS in
        0:rpmstuff)             echo '-L -l -g -a -C -N -S -R -w -s --ssh-creds --nocheck --only-rpms --only-srpms --workdir --storage --list-jobs --list-groups --group --all-jobs --kill-cache'
                                mkittool rpmstuff -l ;;
        1:-a|1:--all)           mkittool rpmstuff -L ;;
    esac
}
